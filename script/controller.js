// Convert number to money VND
function convertMoney(num) {
    return num.toLocaleString("it-IT", { style: "currency", currency: "VND" });
  }

// function render product from database(MockAPI) to screen
function renderThongTin(array) {
  var contentHTML = "";
  array.forEach(function (item) {
    var content = `
      <div class="products__card col-lg-2 col-sm-12 col-md-4">
        <div class="products__desc">
          <h4 class="title" id="title">${item.name}</h4>
          <hr>
          <p class="price" id="price">Price: ${convertMoney(Number(item.price))}</p> 
          <img id="img" src="${item.img}" alt="product img">
          <div class="products__desc_overlay">
            <p class="screen">Screen: <i>${item.screen}</i></p>
            <p class="backCamera">Back Camera: <i>${item.backCamera}</i></p>
            <p class="frontCamera">Front Camera: <i>${item.frontCamera}</i></p>
            <p class="desc">Description: <i>${item.desc}</i></p>
            <div onclick="addToCart(${item.id})" class="toCart" title="Add to Cart" data-toggle="modal"
            data-target="#myModal"><span>Add to Cart</span><i class="fa-solid fa-cart-plus"></i></div>
          </div> 
        </div> 
      </div>
      `;
    contentHTML += content;
  });
  $(".products__list").innerHTML = contentHTML;
}

// 2. function render screen to cartlist
function renderCart(array) {
  var contentHTML = "";
  array.forEach(function (item) {
    var content = `
    <tr class="cart-item">
        <td class="cart-id" id="cart-id">${item.itemID}</td>
        <td class="cart-img" id="cart-img"> <img src=${item.itemImg} alt="image"></td>
        <td class="cart-name" id="cart-name">${item.itemName}</td>
        <td class="cart-price" id="cart-price">${convertMoney(Number(item.itemPrice))}</td>
        <td class="cart-quantity" id="cart-quantity">
        <div id="subQuantity" class="btn-qty" title="Decrease quantity by 1" onclick="qtySub(${item.itemID})"><i class="fas fa-chevron-left"></i></div>
        <p class="qty">${item.itemQuantity}</p>
        <div id="addQuantity" class="btn-qty" title="Increase quantity by 1" onclick="qtyAdd(${item.itemID})"><i class="fas fa-chevron-right"></i></div>
        </td>
        <td>
        <div id="cart-remove" class="cart-remove" onclick="removeCart(${item.itemID})" title="Remove this item"><i class="fa-solid fa-trash-can"></i></div>
        </td>
    </tr>
    `;
    contentHTML += content;
  });
  $("#tbody-cart").innerHTML = contentHTML;
}

// function total cost
function renderTotalCost(array) {
  let totalCost = 0;
  array.forEach(function (item) {
    const product = item;
    totalCost += product.itemQuantity * product.itemPrice;
  });
  $("#totalCost").innerText = convertMoney(Number(totalCost));
}

// function substract quantity
function qtySub(idSelected){
  var iconQuantitySub = $('#subQuantity i');
  CartList.forEach((item) => {
    if (item.itemID == idSelected){
      if (item.itemQuantity > 1){
        iconQuantitySub.style.cursor = 'allowed';
        item.itemQuantity--;  
        // renderCart(CartList);
        // updateTotalCartQuantity(CartList);
      }else if (item.itemQuantity == 1){
        iconQuantitySub.style.cursor = 'not-allowed';
        iconQuantitySub.style.backgroundcolor = 'red';
        // renderCart(CartList);
        // updateTotalCartQuantity(CartList);
      }
    }

  })
  renderCart(CartList);
  renderTotalCost(CartList);
  updateTotalCartQuantity(CartList);
  saveToLocalStorage();
};

// function add quantity
function qtyAdd(idSelected){
  CartList.forEach((item) => {
    if (item.itemID == idSelected){
      item.itemQuantity++;
      // renderCart(CartList);
      // updateTotalCartQuantity(CartList);
    } 
  })
  renderCart(CartList);
  renderTotalCost(CartList);
  updateTotalCartQuantity(CartList);
  saveToLocalStorage();
};

// function remove cart
function removeCart(idSelected){
  CartList.forEach((item) => {
    var index = CartList.indexOf(item)
    if (CartList.length > 0){
      if (item.itemID == idSelected){
        CartList.splice(index, 1) ;
        console.log('Index removed: ', index);
        
        // renderCart(CartList);
        // updateTotalCartQuantity(CartList);
        // updateTotalCartQuantity(CartList);
        console.log('Object removed: ', item);
        console.log('CartList after removed: ', CartList);
        console.log('CartList length: ',CartList.length);
      } 
    }
  })
  renderCart(CartList);
  updateTotalCartQuantity(CartList);
  renderTotalCost(CartList);
  saveToLocalStorage();
};

// function view cart from big icon
function viewCart(){
  renderCart(CartList);
  if (CartList.length == 0){
    emptyCartMsg.innerText = 'Danh mục giỏ hàng đang trống. Mời bạn mua hàng!';
  }else{
    emptyCartMsg.innerText = '';
  }
}

// function count total quantity in cart
function updateTotalCartQuantity(array) {
  let totalQuantity = 0;
  array.forEach(function (item) {
    const product = item;
    totalQuantity += product.itemQuantity;
  });
  $("#totalQuantity").innerText = Number(totalQuantity);
  $('#cart-info-message').innerText = `There are ${totalQuantity} item(s) in shopping list!`;
}

// function purchase
function purchasePhone(){
  let shoppingList = CartList.length;
  // remove item from index 0 to array length, mean all items
  CartList.splice(0, shoppingList);
  renderCart(CartList);
  updateTotalCartQuantity(CartList);
  renderTotalCost(CartList);
  saveToLocalStorage();
  // sweet message
  Swal.fire(
    'Purchased successfully!',
    'Thank you and see you again soon',
    'success'
  )
}

// function clear cart
function clearCart(){
  let shoppingList = CartList.length;
  // remove item from index 0 to array length, mean all items
  CartList.splice(0, shoppingList);
  renderCart(CartList);
  updateTotalCartQuantity(CartList);
  renderTotalCost(CartList);
  saveToLocalStorage();
  // sweet message
  Swal.fire(
    'Shopping list in cart cleared!',
    'Have you not found relevant phone? Happy shopping!',
    'question'
  )
}
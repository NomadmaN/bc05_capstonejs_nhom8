// binding DOM command
const $ = document.querySelector.bind(document);
// local storage
const CyberPhoneCartItems = 'CyberPhone Cart Items';
// empty cart message
const emptyCartMsg = $('#empty-cart-message');  
// axios url
const BASE_URL = "https://63652c83046eddf1bae74845.mockapi.io";
// arrays list
let PhoneList = [];
let PhoneListFiltered = [];
let CartList = [];


// load all data at loading
fetchAllData();


// load data from axios
function fetchAllData(){
    // loadingOn();
    // lấy thông tin từ mockAPI
    axios ({
        url: `${BASE_URL}/Products`,
        method: 'GET', 
    })
    .then(
        function (res) {
            // loadingOff();
            console.log('Data fetched from Axios: ',res.data);
            
            // push data vào PhoneList
            res.data.forEach(function(element){
                PhoneList.push(element);
                console.log('PhoneList: ', PhoneList);
                // render ra màn hình
                renderThongTin(PhoneList);
                updateTotalCartQuantity(CartList);
            })
        })
    .catch(
        function (err) {
            // error message
    });
}

// search phone type
$('#phoneType').addEventListener('change', function(){
    var phoneTypeSelect = $('#phoneType').value;
    if (phoneTypeSelect === 'Iphone'){
        PhoneListFiltered = PhoneList.filter(function(element){
            return element.type === 'Iphone';
        })
        renderThongTin(PhoneListFiltered);
        console.log('PhoneListFiltered: ', PhoneListFiltered);
    }else if (phoneTypeSelect === 'Samsung'){
        PhoneListFiltered = PhoneList.filter(function(element){
            return element.type === 'Samsung';
        })
        renderThongTin(PhoneListFiltered);
        console.log('PhoneListFiltered: ', PhoneListFiltered);
    }else if (phoneTypeSelect === 'Oppo'){
        PhoneListFiltered = PhoneList.filter(function(element){
            return element.type === 'Oppo';
        })
        renderThongTin(PhoneListFiltered);
        console.log('PhoneListFiltered: ', PhoneListFiltered);
    }else if (phoneTypeSelect === 'Xiaomi'){
        PhoneListFiltered = PhoneList.filter(function(element){
            return element.type === 'Xiaomi';
        })
        renderThongTin(PhoneListFiltered);
        console.log('PhoneListFiltered: ', PhoneListFiltered);
    }else if (phoneTypeSelect === 'Vivo'){
        PhoneListFiltered = PhoneList.filter(function(element){
            return element.type === 'Vivo';
        })
        renderThongTin(PhoneListFiltered);
        console.log('PhoneListFiltered: ', PhoneListFiltered);
    }else if (phoneTypeSelect === 'Realme'){
        PhoneListFiltered = PhoneList.filter(function(element){
            return element.type === 'Realme';
        })
        renderThongTin(PhoneListFiltered);
        console.log('PhoneListFiltered: ', PhoneListFiltered);
    }else if (phoneTypeSelect === 'Nokia'){
        PhoneListFiltered = PhoneList.filter(function(element){
            return element.type === 'Nokia';
        })
        renderThongTin(PhoneListFiltered);
        console.log('PhoneListFiltered: ', PhoneListFiltered);
    }else{
        renderThongTin(PhoneList);
    }
})

// add to cart for chosen phone
function addToCart(idSelected){
    // set phone đang nhấn là 1 object
    let currentPhone = {};
    // giá trị của object này bằng chính object trong PhoneList nếu trùng ID
    PhoneList.forEach((element) => {
        if (element.id == idSelected){
            currentPhone = element;
            console.log('currentPhone :', currentPhone);
        }
    });    
    // create object cartItem from currentPhone with quantity = 1
    let cartItem = new CartItem(
        currentPhone.id,
        currentPhone.img,
        currentPhone.name,
        currentPhone.price,
        1
        );
    console.log('cartItem :', cartItem);
    // nếu trong cart đã có item
    if (CartItem.length != 0){
        let cartItemAvailable = false;
        CartList.forEach((element) => {
            // và item trong cart có id trùng với id phone đang chọn/nhấn
            if (element.itemID == currentPhone.id){
                // thì cộng thêm số lượng cho phone trng cart có id trùng đó
                element.itemQuantity++;
                cartItemAvailable = true;
                // render lại để quantity update
                emptyCartMsg.innerText = '';
                // renderCart(CartList);
                // renderTotalCost(CartList);
                // updateTotalCartQuantity(CartList);
            }
        });
        // nếu trong cart chưa có item
        if (!cartItemAvailable){
            // thì push bình thường vào trong cart với mặc định quantity = 1
            CartList.push(cartItem);
            emptyCartMsg.innerText = '';
            // renderCart(CartList);
            // renderTotalCost(CartList);
            // updateTotalCartQuantity(CartList);
        }
    }else{
        CartList.push(cartItem);
        emptyCartMsg.innerText = '';
        // renderCart(CartList);
        // renderTotalCost(CartList);
        // updateTotalCartQuantity(CartList);
    }
    renderCart(CartList);
    renderTotalCost(CartList);
    updateTotalCartQuantity(CartList);
    saveToLocalStorage();
};

// local storage save
function saveToLocalStorage(){
    let jsonCartList = JSON.stringify(CartList);
    console.log('Shopping list được lưu vào Storage: ', jsonCartList);
    localStorage.setItem(CyberPhoneCartItems, jsonCartList);
}
// local storage load
let dataJSON = localStorage.getItem(CyberPhoneCartItems);
if (dataJSON !== null){
    let arrayTemp = JSON.parse(dataJSON);
    for (let i = 0; i < arrayTemp.length; i++) {
        let phoneTemp = arrayTemp[i];
        let phoneSaved = new CartItem (
            phoneTemp.itemID,
            phoneTemp.itemImg,
            phoneTemp.itemName,
            phoneTemp.itemPrice,
            phoneTemp.itemQuantity
        );
        CartList.push(phoneSaved);
        console.log('Shopping list loaded từ Storage: ', CartList);
    }
    renderCart(CartList);
}

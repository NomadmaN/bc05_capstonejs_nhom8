// binding DOM command
const $ = document.querySelector.bind(document);
// database url
const BASE_URL = "https://63652c83046eddf1bae74845.mockapi.io";

// Convert number to money VND
function convertMoney(num) {
    return num.toLocaleString("it-IT", { style: "currency", currency: "VND" });
}

// loading function
function loadingOn(){
    $('#loading').style.display = "flex";
}

function loadingOff(){
    $('#loading').style.display = "none";
}

// fetch on loading
fetchAllData();

// function fetch all on loading
function fetchAllData(){
    loadingOn();
    // get info from mockAPI
    axios ({
        url: `${BASE_URL}/Products`,
        method: 'GET', 
    })
    .then(
        function (res) {
            loadingOff();
            console.log('Data fetched from mockAPI: ', res.data);
            renderCyberPhone(res.data);
        })
    .catch(
        function (err) {
            // error message
    });
}

// function render database
function renderCyberPhone(array){
    var contentHTML = '';
    array.forEach(function(item){
        var content = 
        `
        <tr>
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>${convertMoney(Number(item.price))}</td>
            <td>${item.screen}</td>
            <td>${item.backCamera}</td>
            <td>${item.frontCamera}</td>
            <td class="db-img"><img src="${item.img}" alt="product img"></td>
            <td>${item.desc}</td>
            <td>${item.type}</td>
            <td class="setting">
                <span style="cursor: pointer" class="btn btn-danger" onclick="removePhone(${item.id})" title="Delete"><i class="fa-solid fa-xmark"></i></span>
                <span style="cursor: pointer" class="btn btn-warning" onclick="editPhone(${item.id})" title="Edit" data-toggle="modal"
                data-target="#adminModal"><i class="fa-solid fa-wrench"></i></span>
            </td>
        </tr>
        `;
        contentHTML += content;
    });
    $('#tableCyberPhone').innerHTML = contentHTML;
}

// function get info from input form
function getFormInformation(){
    var nameInput = $('#phoneName').value;
    var priceInput = $('#phonePrice').value;
    var screenInput = $('#phoneScreen').value;
    var backCameraInput = $('#phoneBCamera').value;
    var frontCameraInput = $('#phoneFCamera').value;
    var imageInput = $('#phoneImage').value;
    var descInput = $('#phoneDesc').value;
    var typeInput = $('#phoneType').value;
    return {
        name: nameInput,
        price: priceInput,
        screen: screenInput,
        backCamera: backCameraInput,
        frontCamera: frontCameraInput,
        img: imageInput,
        desc: descInput,
        type: typeInput,
    }
}

// function push info edit to form
function pushEditInformation(item){
    $('#phoneName').value = item.data.name;
    $('#phonePrice').value = item.data.price;
    $('#phoneScreen').value = item.data.screen;
    $('#phoneBCamera').value = item.data.backCamera;
    $('#phoneFCamera').value = item.data.frontCamera;
    $('#phoneImage').value = item.data.img;
    $('#phoneDesc').value = item.data.desc;
    $('#phoneType').value = item.data.type;
}

// function add phone
function addPhone(){ 
    // DOM
    var dataInput = getFormInformation();
    // New object
    var newPhone = {
        name: dataInput.name,
        price: dataInput.price,
        screen: dataInput.screen,
        backCamera: dataInput.backCamera,
        frontCamera: dataInput.frontCamera,
        img: dataInput.img,
        desc: dataInput.desc,
        type: dataInput.type,
    };
    // check valiadation
    var isValid = true;
    isValid =
    checkNull(dataInput.name,'#tbPhoneName') &&
    checkInputLength(dataInput.name,1,20,'#tbPhoneName');
    isValid = isValid &
    (checkNull(dataInput.price, '#tbPhonePrice') &&
    checkInputLength(dataInput.price,6,8,'#tbPhonePrice'));
    isValid = isValid &
    (checkNull(dataInput.screen, '#tbPhoneScreen') &&
    checkInputLength(dataInput.screen,1,30,'#tbPhoneScreen'));
    isValid = isValid &
    (checkNull(dataInput.backCamera, '#tbPhoneBCamera') &&
    checkInputLength(dataInput.backCamera,1,40,'#tbPhoneBCamera'));
    isValid = isValid &
    (checkNull(dataInput.frontCamera, '#tbPhoneFCamera') &&
    checkInputLength(dataInput.frontCamera,1,40,'#tbPhoneFCamera'));
    isValid = isValid &
    (checkNull(dataInput.desc, '#tbPhoneDesc') &&
    checkInputLength(dataInput.desc,1,100,'#tbPhoneDesc'));
    isValid = isValid &
    checkNull(dataInput.img, '#tbPhoneImage');
    isValid = isValid &
    checkNull(dataInput.type, '#tbPhoneType');

    if (isValid){
        loadingOn();
        //  add = POST
        axios ({
            url: `${BASE_URL}/Products`,
            method: 'POST',
            // khai báo object mới thêm vào từ object DOM newAPI
            data: newPhone,  
        })
        .then(
            function (res) {
                // loadingOff();
                console.log('Phone added: ', res.data);
                // reload all
                fetchAllData();
                Swal.fire(
                    'New phone added successfully!',
                    'Close this modal to see the new added phone.',
                    'success'
                )
            })
        .catch(
            function (err) {
                // error message
        });
    }else{
        // querySelectorAll on selector is an array, make loop to cover all
        const showError = document.querySelectorAll(".modal-body .form-group .sp-thongbao");
        for (let i = 0; i < showError.length; i++) {
            showError[i].style.display = 'block';
        }
        loadingOff();
    }
}

// function remove phone
function removePhone(idSelected){
    loadingOn();
    axios ({
        url: `${BASE_URL}/Products/${idSelected}`,
        method: 'DELETE',  
    })
    .then(
        function (res) {
            // loadingOff();
            console.log('Phone removed: ', res.data);
            // reload all
            fetchAllData();
        })
    .catch(
        function (err) {
            // error message
    });
}

// function edit phone
function editPhone(idSelected){
    loadingOn();
    axios ({
        url: `${BASE_URL}/Products/${idSelected}`,
        method: 'GET',  
    })
    .then(
        function (res) {
            console.log('Phone to edit: ', res.data);
            // update id edit
            idEdited = res.data.id;
            // push info to form
            pushEditInformation(res);
            loadingOff();
        })
    .catch(
        function (err) {
            // error message
    });
}

// function update phone
function updatePhone(){
    loadingOn();
    // DOM
    let dataEdited = getFormInformation();
    // New object
    var editedPhone = {
        name: dataEdited.name,
        price: dataEdited.price,
        screen: dataEdited.screen,
        backCamera: dataEdited.backCamera,
        frontCamera: dataEdited.frontCamera,
        img: dataEdited.img,
        desc: dataEdited.desc,
        type: dataEdited.type,
    };
    // check valiadation
    var isValid = true;
    isValid =
    checkNull(dataEdited.name,'#tbPhoneName') &&
    checkInputLength(dataEdited.name,1,20,'#tbPhoneName');
    isValid = isValid &
    (checkNull(dataEdited.price, '#tbPhonePrice') &&
    checkInputLength(dataEdited.price,6,8,'#tbPhonePrice'));
    isValid = isValid &
    (checkNull(dataEdited.screen, '#tbPhoneScreen') &&
    checkInputLength(dataEdited.screen,1,30,'#tbPhoneScreen'));
    isValid = isValid &
    (checkNull(dataEdited.backCamera, '#tbPhoneBCamera') &&
    checkInputLength(dataEdited.backCamera,1,40,'#tbPhoneBCamera'));
    isValid = isValid &
    (checkNull(dataEdited.frontCamera, '#tbPhoneFCamera') &&
    checkInputLength(dataEdited.frontCamera,1,40,'#tbPhoneFCamera'));
    isValid = isValid &
    (checkNull(dataEdited.desc, '#tbPhoneDesc') &&
    checkInputLength(dataEdited.desc,1,100,'#tbPhoneDesc'));
    isValid = isValid &
    checkNull(dataEdited.img, '#tbPhoneImage');
    isValid = isValid &
    checkNull(dataEdited.type, '#tbPhoneType');

    if (isValid){
        axios ({
            url: `${BASE_URL}/Products/${idEdited}`,
            method: 'PUT', 
            data: editedPhone,
        })
        .then(
            function (res) {
                loadingOff();
                console.log('Phone updated: ', res.data);
                // reload all
                fetchAllData();
                Swal.fire(
                    'Phone edited successfully!',
                    'Close this modal to see updated information.',
                    'success'
                )
            })
        .catch(
            function (err) {
                // error message
        });
    }else{
        // querySelectorAll on selector is an array, make loop to cover all
        const showError = document.querySelectorAll(".modal-body .form-group .sp-thongbao");
        for (let i = 0; i < showError.length; i++) {
            showError[i].style.display = 'block';
        }
        loadingOff();
    }
}
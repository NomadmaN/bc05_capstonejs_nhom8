// function create model CartItem
function CartItem (itemID,itemImg,itemName,itemPrice,itemQuantity){
    this.itemID = itemID;
    this.itemImg = itemImg;
    this.itemName = itemName;
    this.itemPrice = itemPrice;
    this.itemQuantity = itemQuantity;
}
// function check null
function checkNull(userInput,resultID){
    if (userInput.length == 0 || Number(userInput) == 0){
        $(resultID).innerHTML = 'Vui lòng không để trống!';
        return false;
    }else{
        $(resultID).innerHTML = '';
        return true;
    }
}
function checkInputLength(userInput,minlength,maxlength,resultID){
    var minLen = minlength;
    var maxLen = maxlength;
    if (userInput.length >= minLen && userInput.length <= maxLen){
        $(resultID).innerHTML = '';
        return true;
    }else{
        $(resultID).innerHTML = 'Vui lòng nhập trong khoảng ' + minLen + ' tới ' + maxLen + ' ký tự!';
        return false;
    }
}

